# Double DQN

Lab3 of the [Deep RL Bootcamp.](https://sites.google.com/view/deep-rl-bootcamp/lectures)

- [Double DQN Slides](https://drive.google.com/file/d/0BxXI_RttTZAhVUhpbDhiSUFFNjg/view?usp=sharing&resourcekey=0-RVIQ5RKnXVphXgiS-NAeZw)

---

## Task 3.1 - 3.3

Pleas see the `simpledqn/main.py` file for this.

## 3.4 Learning Pong with RAM Observation

Below are the plots using the default parameters for Pong. We observe the desired behavior as the average return as well as the discounted return increase while the temporal difference error decreases.

#### Average Return Baseline
![ar](figs/default/dqn_pong_average_return.png)

#### Average Discounted Return Baseline
![ard](figs/default/dqn_pong_average_discounted_return.png)

#### Temporal Difference Error Baseline
![td](figs/default/dqn_pong_average_td_error.png)


The default parameters for the DQN are visible below.

```python
dqn = DQN(
        env=env,
        get_obs_dim=get_obs_dim,
        get_act_dim=get_act_dim,
        obs_preprocessor=obs_preprocessor,
        replay_buffer=replay_buffer,

        # Q-value parameters
        q_dim_hid=[256, 256] if env_id == 'Pong-ram-v0' else [],
        opt_batch_size=64,

        # DQN gamma parameter
        discount=0.99,

        # Training procedure length
        initial_step=initial_step,
        max_steps=max_steps,
        learning_start_itr=max_steps // 100,
        # Frequency of copying the actual Q to the target Q
        target_q_update_freq=1000,
        # Frequency of updating the Q-value function
        train_q_freq=4,

        # Exploration parameters
        initial_eps=1.0,
        final_eps=0.05,
        fraction_eps=0.1,

        # Logging
        log_freq=log_freq,
        render=render,
    )
```

### Lower `gamma` (discount factor)

Instead if a discount factor of 0.99 I am using 0.8.

#### Average Return

![ar](figs/gamma/discount_08_average_return.png)

Decreasing the discount factor decreases the effective model horizon.  As visible in the plot of the average return above reduced discount factor did not work well. The average return dropped and stagnated at this lower value indicating that the agent does not learn when using this hyperparameter. Therefore further experiments should be conducted with at least 0.8 discount factor, the chosen value here unfortunately is too low.

### Lower `train_q_freq` (Frequency of updating the Q-value function)

In order to change the temporal difference learning behavior we first change the `train_q_freq=4` to 2. This should update the Q function after every 2 steps (which is a bit counterintuitive since lower values correspond to higher frequency here).

#### Average Return

![ar](figs/train_freq/train_freq_2_average_return.png)

#### Temporal Difference Error

![td](figs/train_freq/train_freq_2_tderror.png)

This actually decreased training speed by a factor of around 2 and therefore the model was much slower in learning which is why there are also less epochs available. The result was therefore decreased training speed while the average return given the epoch and the temporal difference error seem comparable to the baseline.

### Increase `train_q_freq` (Frequency of updating the Q-value function)

In order to change the temporal difference learning behavior we first change the `train_q_freq=4` to 6. This should update the Q function after every 6 steps.

#### Average Return

![ar](figs/train_freq/train_freq_6_average_return.png)

#### Temporal Difference Error

![td](figs/train_freq/train_freq_6_tderror.png)

This actually increased training speed compared to the default baseline. The average return as well as the temporal difference error seem slightly lower. This seems surprising to me since we are giving the Q Values more steps to diverge, I would have expected the opposite here.

### Lower `target_q_update_freq` (Frequency of copying the actual Q to the target Q)

I lowered the `target_q_update_freq` to 500 compared to the 1000 in the baseline. The parameter updates the target Q-value function by copying the current Q-value function weights. As above lowering the value actually increases the frequency.

#### Average Return

![ar](figs/update_freq/update_freq_500_average_return.png)

#### Temporal Difference Error

![td](figs/update_freq/update_freq_500_tderror.png)

When comparing the experiment with the baseline we can see that the variance of the average return seems lower. This makes sense since we are updating the Q value target function more frequently which reduces the differences between Q-value and Q-target-value and therefore smoothes the training process. This is also visible in the temporal difference error which is generally lower in this experiment when compared to the baseline.


### Increase `target_q_update_freq` (Frequency of copying the actual Q to the target Q)

I increased the `target_q_update_freq` to 2000 compared to the 1000 in the baseline. Compared to the baseline I cannot make out much difference in the average return values. The achieved temporal difference error however is higher when compared to the baseline as well as the experiment above which makes sense since we are running more training steps until we update the target Q function leading to a higher error rate.

#### Average Return

![ar](figs/update_freq/update_freq_2000_average_return.png)

#### Temporal Difference Error

![td](figs/update_freq/update_freq_2000_tderror.png)
