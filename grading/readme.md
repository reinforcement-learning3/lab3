# Grading of Lab3

[Link to graded repo](https://github.zhaw.ch/siegelou/EVA_MI_Labs/tree/main/lab3)

## Task Completion

- [x] `compute_q_learning_loss` -  Completed
- [x] `compute_double_q_learning_loss`
- [x] `double_q_learning_update`

## Review

All tasks were completed and annotated with comments in the code.

In `compute_q_learning_loss` as well as the `compute_double_q_learning_loss` function the terminal states are treated less cryptically compared to my code.
Instead of using F.select on a stacked columns of q_value and reward in order to distinguish between terminal and non-terminal states the discounted rewards are simply multiplied with $`(1-l_{done})`$ setting the discounted rewards to 0 whenever `l_done` contains a 1 which indicates a terminal state. For reasons of simplicity i therefore favor Louis approach compared to mine.

## Experiments

[Experiments DOCX](https://github.zhaw.ch/siegelou/EVA_MI_Labs/blob/main/lab3/results_lab3.docx)

The results show the plots of the different experiments implemented in the lab.

What i am missing is an explanation of the expected changes as well as an (possible) interpretation of why the chosen hyperparameters might work better when compared to the baseline.

## Final grade

I would grade the lab with a **5.5** since all experiments and tasks were fulfilled though as mentioned beforehand I am missing a more detailed explanation of the hyperparameter changes performed in the experiments as well as a more detailed display of a comparison of the baseline network together with its parameters to the experimental changes.
